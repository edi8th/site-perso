<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>B.D.D.G.E.M.P.B.d'A</title>




    <style media="screen">
    @font-face {
      font-family: 'Fragen';
      font-style: normal;
      font-weight: 300;
      src: url("fonts/Fragen-LightItalic.woff") format('woff2');
    }
    @font-face{
      font-family: 'Michaux';
      font-style: normal;
      font-weight: 300;
      src: url("fonts/michaux-bold.woff2") format('woff2');
    }
    @-webkit-keyframes rotating /* Safari and Chrome */ {
      from {
        -webkit-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      to {
        -webkit-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }
    @keyframes rotating {
      from {
        -ms-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -webkit-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      to {
        -ms-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -webkit-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    @-webkit-keyframes respiration /* Safari and Chrome */ {
      from {
        -webkit-letter-spacing: 0em;
        -o-letter-spacing: 0em;
        letter-spacing: 0em;
      }
      to {
        -webkit-letter-spacing: 1em;
        -o-letter-spacing: 1em;
        letter-spacing: 1em;
      }
    }
    @keyframes respiration {
      from {
        -ms-letter-spacing: 0em;
        -moz-letter-spacing: 0em;
        -webkit-letter-spacing: 0em;
        -o-letter-spacing: 0em;
        letter-spacing: 0em;
      }
      to {
        -ms-letter-spacing: 1em;
        -moz-letter-spacing: 1em;
        -webkit-letter-spacing: 1em;
        -o-letter-spacing: 1em;
        letter-spacing: 1em;
      }
    }

    .alorsquoiquoi {
      -webkit-animation: rotating 104s linear infinite;
      -moz-animation: rotating 104s linear infinite;
      -ms-animation: rotating 104s linear infinite;
      -o-animation: rotating 104s linear infinite;
      animation: rotating 104s linear infinite;
      transition: .4s filter;
      position: fixed;
      top: 2em;
      left: 2.5em;
      z-index: 1;
      transition: 2s left, 1s width;
      width: 10%;
      display: block;
      margin-bottom:1.4em;
      cursor: pointer;
    }

    .ouhouh{
      left: 86vw !important;
    }

    .lamenuiserie{
      width: 5% !important;;
    }
    b{
      font-family: 'Michaux';
      font-size: 1.8vw;
      display: block;
      font-style: italic;
      /* -webkit-animation: respiration 100s linear infinite;
      -moz-animation: respiration 100s linear infinite;
      -ms-animation: respiration 100s linear infinite;
      -o-animation: respiration 100s linear infinite;
      animation: respiration 100s linear infinite; */
      transition: color 10s;
      cursor: pointer;
    }
    b:hover{
      color:#f6dce2;
    }
    a{
      font-family: 'Michaux';
      font-size: 1.45vw;
      color:inherit;
      text-decoration: none;
      color:#e94822;
      transition: 4s color;
    }
    a:hover{
      color:blue;
    }
    body{
      font-size: 1.4vw;
      line-height: 2.2vw;
      color: #E1E1E1;
      font-family: "Fragen";
      overflow-x: hidden;
      overflow-y: scroll;
}

@-webkit-keyframes Gradient {
	0% {
		background-position: 0% 50%
	}
	50% {
		background-position: 100% 50%
	}
	100% {
		background-position: 0% 50%
	}
}

@-moz-keyframes Gradient {
	0% {
		background-position: 0% 50%
	}
	50% {
		background-position: 100% 50%
	}
	100% {
		background-position: 0% 50%
	}
}

@keyframes Gradient {
	0% {
		background-position: 0% 50%
	}
	50% {
		background-position: 100% 50%
	}
	100% {
		background-position: 0% 50%
	}
}
    #dassaut{
        position: fixed;
        bottom: 2em;
        left: 2.5em;
        z-index: -0;
        width: 50vw;
        transition: 2s left;
    }
    #dassaut p{
        margin-top:0;
        margin-bottom:0;
        text-indent: 2em;
    }
    #dassaut p img{
      width: 4vw;
      margin-bottom: -.1em;
    }
    #dassaut p img:hover{
     }
    #jenefaisquepasser{
      width: 100vw;
      height: 100vw;
      position: absolute;
      transition: 5s left, 5s top;
      left:0vw;
      top:0vw;
      z-index: -9;
    }
    #jenefaisquepasser img{
      width: 100%;
    }
    .fl{
      color:#e94822;
      line-height: 1.5em;
    }
    .leslienssûrs{
      position: inherit;
      display: inline-block;
      font-size: 1em;
      left:0;
      width: auto;
      margin-top: 0;
    }
    .lame{
      left: 100em !important;
    }
    #dunerfoptique{
      position: absolute;
      right: 120em;
      z-index: -0;
      width: 80vw;
      transition: 2s right;
      padding-bottom: 4em;
      top:1.8em;
    }
    #dunerfoptique img{
      display: none;
    }
    #dunerfoptique p{
      margin-bottom: 1em;
      margin-top: -2em;
      font-family: 'Michaux';
      display: none;
      font-size: .5em;
    }
    .lamelle{
      right: -2.3vw !important;
    }

    .lamelle2{
      left: 130vw !important;
    }
    section section{
      margin-top: 2em;
    }
    .akopita{
      font-family: 'Michaux';
      font-size: .6em;
    }
    /* .akopita:hover{
      font-family: 'Fragen';
      font-size: .8em;
      line-height: 2.55em;
    } */
    ul{
      list-style:none;
      margin-left:0;
      padding-left:0;
      line-height: 1.35em;
      color:color:#e94822;
    }
    ul a{
      font-family: "Fragen";
    }
    /* ul li:nth-child(even){
      margin-left: 3em;
    } */
    #lesputainsdetrucsquifontcequeceputainsite{
      padding-top: 2em;
    }
    #lesputainsdetrucsquifontcequeceputainsite a{
      font-family: "Fragen";
    }
    #lamedesputainsdecartes{
      background: lightblue;
      position: fixed;
      top:0;
      left:0;
      width: 100vw;
      height: 100vh;
      background: linear-gradient(169deg, #2a3500 0%, #201301 100%);
      background-size: 400% 400%;
      -webkit-animation: Gradient 51s ease infinite;
      -moz-animation: Gradient 51s ease infinite;
      animation: Gradient 51s ease infinite;
    }
    .test{
      position: absolute;
      color:white;
      left:8em;
      top: 2em;
      font-size: 1em;
      display: none;
    }
    ul ul{
      margin-left: 2em;
      transition: .5s margin-left;
    }
    ul ul{
      display: none;
    }
    .test li:hover{
      cursor: pointer;
      color:#e94822;
    }
    .test li{
    overflow: hidden;
    border-bottom:1px solid white;
    }
    .here{
      color:#e94822;
      font-family: 'Michaux';
    }
    .nomargins{
      margin-left: 0 !important;
    }
    .phantom{
      width: 50vw;
      position: absolute;
      top:10vh;
      left:30vw;
    }
    .phantom img{
      width: 100%;
    }
    .boutonboul span{
      color:#e94822;
      transition: .5s color;
    }
    .boutonboul span:hover{
      color:white;
      cursor: pointer;
    }
    .boutonboul{
      position: fixed;
      bottom:2em;
      right: 2em;
      text-align: right;
    }
    .grid-sizer,
    .grid-item{
      width: 24%;
      margin-bottom: 22px;
    }
    .grid-sizer2,
    .grid-item2 {
      width: 32%;
      margin-bottom: 22px;
    }

    .grid-item div{
      font-size: .3em;
      text-align: center;
      font-family: 'Michaux';
      display: none;
    }
    .grid-item,
    .grid-item2 {
      float: left;
      /* transition: .2s width; */
    }

    .grid-itemopen{
      width: 74.5%;
    }
    .grid-item img,
    .grid-item2 img {
      display: block;
      width: 100%;
    }
    .grid-item img:hover{
      cursor: pointer;
    }
    #retour{
      margin-bottom: -1em;
    }
    #retour:hover{
      color:#e94822;
      cursor: pointer;
    }
    #txt{
      width: 60vw;
      position: absolute;
      top:6vh;
      left:20vw;
      font-size: 1.4em;
      display: none;
      transition: 2s left;
    }
    #txt a{
      font-size: 1.4em;
    }

#babel{
  position: absolute;
  left: 13em;
  z-index: 2;
  width: 60vw;
  top:1.8em;
}
#introbiblio{
  position: fixed;
  right: 2em;
  width: 7vw;
  text-align: right;
  display: none;
}
#introoptique{
  position: fixed;
  right: 2em;
  top:19em;
  width: 3vw;
  text-align: right;
  display: none;
}
.lefty{
  text-align: left;
  display: block;
}

    </style>
  </head>
  <body>
    <section id="babel" class="gridbabel">
      <div id="introbiblio">Petits selections de livres <span class="lefty"> que si vous ne les avez pas lu,</span> c'est pas con de le faire.</div>
      <div class="grid-sizer2"></div>
      <?php
        $directory = "babel";
        $images = glob($directory . "/*.*");
        shuffle($images);
        foreach($images as $image)
        {
          $image_info = getimagesize($image);

            echo '<div class="grid-item2" data-height="'.$image_info[1].'" data-width="'.$image_info[0].'">';
            echo '<img class="lazy" data-src="'.$image.'" alt="">';
            echo '</div>';
        }
      ?>
    </section>

    <div id="lamedesputainsdecartes"></div>
    <img class="alorsquoiquoi" src="tuveux/testerstupeflip3.svg" vert="testerstupeflip3.svg" rouge="gangdesmotards2.svg">
    <div class="phantom">
      <img src="" alt="">
    </div>
    <span class="boutonboul"> <span id="txtappear">Textes</span> <br> <span id="masonryappear">Travail</span> <br> <span id="biblioappear">Lecture(s</span>  </span>

    <section id="dassaut">
        <b>Benjamin Dumond</b>
        Graphiste libriste, je suis un organe du groupuscule <a class="fl" href="http://www.bonjourmonde.net">Bonjour Monde</a>.

        <p>
          J'écris pour <a class="fl" href="http://www.conjonction.org">Conjonction</a>, sur les liens entre magie mystique <br>et typographie, ainsi que pour les <a class="fl" href="http://www.strategiesitaliques.fr">Stratégies Italiques</a>, <br>suite d’énonciations abstraites qui visent à libérer, débloquer, décaler<br> la pensée du dessinateur de caractères. Je fais partie de l'organisation<br> des <a class="fl" href="https://www.jdll.org/">Journées du Logiciel Libre</a> de Lyon.<br> Contact via <a class="fl" href="mailto:bonjour@benjamindumond.fr"><img src="tuveux/ct.svg" vert="tuveux/ct2.svg" rouge="tuveux/ct.svg"></a>, scripts via <a href="https://gitlab.com/edi0th"><img src="tuveux/ta.svg" vert="tuveux/ta2.svg" rouge="tuveux/ta.svg"></a>.

        </p>


    </section>
    <section id="txt">
      <ul>
        <li>
          <a class="king" href="http://www.conjonction.org">La typographie a pris la mauvaise pilule</a><br>
          <span class="akopita">Article pour le site conjonction.org. 2019.</i></span>
        </li>
        <li>
          <a class="king" href="http://benjamindumond.fr/ressources/edr-AllWorkAndNoPlayMakesJackADullBoy-2019.pdf">All work and no play makes Jack a dull boy</a><br>
          <span class="akopita">L'Expérience du Récit n°5, <i>ESAB Lorient. 2019.</i></span>
        </li>

        <li>
          <a class="king" href="https://www.esadse.fr/fr/actualites/?news=azimuts-n-48-49-1569">Typographica Hermetica</a><br>
          <span class="akopita">Azimut n°48-49, <i>Le type. Règne, crise et critique. ESAD St-Étienne. 2018.</i></span>
        </li>

        <li>
          <a class="king" href="http://benjamindumond.fr/etc/ressources/Benjamin_Dumond_-_Idees_de_caracteres.pdf">Idée de caractère</a><br>
          <span class="akopita">Mémoire de fin d'étude. ESAD Valence. 2017.</i></span>
        </li>
      </ul>
    </section>

    <section id="dunerfoptique" class="grid">
      <div id="introoptique">tW<span class="lefty">g</span>m<br>,</div>

      <div class="grid-sizer"></div>
      <?php
        $directory = "voute";
        $images = glob($directory . "/*.*");
        // usort($images, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));
        $images = array_reverse($images);
        foreach($images as $image)
        {
          $image_info = getimagesize($image);

            echo '<div class="grid-item" data-height="'.$image_info[1].'" data-width="'.$image_info[0].'">';
            echo '<img class="lazy" data-src="'.$image.'" alt="">';
            echo '</div>';
        }
      ?>
    </section>



    <div class="test">
      <div id="retour">. . /</div>
      <?php

      $results;
      function getDirContents($dir, &$results = array()){
          $files = scandir($dir);
          echo '<ul>';
          foreach($files as $key => $value){
              $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
              if(!is_dir($path)) {
                if (strpos($path, ".jpg") !== false || strpos($path, ".gif") !== false || strpos($path, ".png") !== false || strpos($path, ".jpeg") !== false || strpos($path, ".svg") !== false){
                  echo "<li class='chemin imgsurv' data-url=".$dir.DIRECTORY_SEPARATOR.$value.">".$value."</li>";
                }else{
                  echo "<li class='chemin'>".$value."</li>";
                }
              } else if($value != "." && $value != "..") {
                echo "<li class='chemin aouvrir'>".$value."</li>";
                  getDirContents($path, $results);
              }
          }
          echo '</ul>';
      }

      getDirContents('palace');
      ?>
    </div>
  </body>

  <script src="js/jquery-1.7.1.min.js"></script>
  <script src="js/sisilespainco.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.5.0/dist/lazyload.min.js"></script>
  <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
  <script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.js"></script>
  <script type="text/javascript">


  var lazyLoadInstance = new LazyLoad({
    elements_selector: ".lazy"
});
  </script>
</html>
